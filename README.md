package figuras_geometricas;

/**
 *
 * @author W I C H O
 */
public class Figuras_Geometricas {

    public static void main(String[] args) {
        // Variables y formulas de triangulo
        int m = 12;
        int n = 23;
        int o = m * n / 2;
        // Variables y formulas de Circulo
        int r = 7;
        float Pi = 3.1416f;
        float s = Pi * r * r;
        // Variables y formulas de Cuadrado
        int v = 3;
        int w = v * v;
        // Variables y fomulas de Rectangulo
        int a = 12;
        int b = 19;
        int c = b * a;
        // Imprimir Triangulo
        System.out.println("Formula triangulo [b*h/2]");
        System.out.println("("+m+"*"+n+")"+"/2"+"="+o);
        //Imprimir Circulo
        System.out.println("Formula Circulo [Pi(r*r)]");
        System.out.println(Pi+"("+r+"*"+r+")"+"="+s);
        //Imprimir Cuadrado
        System.out.println("Formula Cuadrado [L*L]");
        System.out.println(v+"*"+v+"="+w);
        //Imprimir Rectangulo
        System.out.println("Formula de Rectangulo[b*h]");
        System.out.println(b+"*"+a+"="+c);
    }
    
}
